require "spec_helper"

describe Lita::Handlers::Echo, lita_handler: true do
  it 'echhoes' do
    send_message('echo tal')
    expect(replies.last).to eq 'tal'
  end
end
