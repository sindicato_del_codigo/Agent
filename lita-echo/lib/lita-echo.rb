require "lita"

Lita.load_locales Dir[File.expand_path(
  File.join("..", "..", "locales", "*.yml"), __FILE__
)]

require_relative "./lita/handlers/echo"

Lita::Handlers::Echo.template_root File.expand_path(
  File.join("..", "..", "templates"),
 __FILE__
)
