FROM ruby:2.5.1

RUN mkdir /code
WORKDIR /code

COPY . /code
RUN rm -f Gemfile.lock

RUN bundle install -j 10

WORKDIR /code/lita-echo
RUN bundle install
